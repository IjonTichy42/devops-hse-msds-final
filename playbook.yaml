---

- hosts: "all"
  become: "yes"

  roles:
    - role: "roles/docker"
      docker_users:
        - "vagrant"

  tasks:

    - name: Gather the package facts
      ansible.builtin.package_facts:
        manager: auto

    - name: "Install minicube"
      ansible.builtin.apt:
        deb: "https://github.com/kubernetes/minikube/releases/download/v1.17.1/minikube_1.17.1-0_amd64.deb"
      when: "'minikube' not in ansible_facts.packages"

    - name: "Add repo keys"
      ansible.builtin.apt_key:
        url: "{{ item }}"
        state: "present"
      with_items:
        - "https://packages.cloud.google.com/apt/doc/apt-key.gpg"

    - name: "Add repositories"
      ansible.builtin.apt_repository:
        repo: "{{ item }}"
        state: present
        update_cache: yes
      with_items:
        - "deb https://apt.kubernetes.io/ kubernetes-xenial main"

    - name: "Install kubectl"
      ansible.builtin.apt:
        pkg: "{{ item }}"
        state: "present"
      with_items:
        - "kubectl"

    - name: "Check if minikube is already running"
      ansible.builtin.command: "minikube status -f '{{ '{{' }} .Host {{'}}'}}'"
      become: no
      changed_when: false
      failed_when: false
      register: minikube_status

    - name: "Launch minikube"
      ansible.builtin.shell:
        cmd: "minikube start"
      when: "not minikube_status.stdout or minikube_status.stdout != 'Running'"
      become: no

    - name: "Set up autocompletion"
      become_user: vagrant
      lineinfile:
        path: "/home/vagrant/.bashrc"
        regexp: "^source <(kubectl completion bash)"
        line: "source <(kubectl completion bash)"

    - name: "Pause for 30 to ensure kubectl is ready"
      pause:
        seconds: 30

    - name: "Start fallback pod"
      ansible.builtin.shell:
        cmd: "kubectl apply -f /vagrant/fallback/fallback_webapp.yaml"
      become: no

    - name: "Start webapp pod"
      ansible.builtin.shell:
        cmd: "kubectl apply -f /vagrant/model/model_webapp.yml"
      become: no
      ignore_errors: yes

    - name: "Expose fallback pod"
      ansible.builtin.shell:
        cmd: "kubectl expose pod fallback --port=5000 --type=NodePort"
      become: no

    - name: "Expose webapp pod"
      ansible.builtin.shell:
        cmd: "kubectl expose pod webapp --port=5000 --type=NodePort"
      become: no
      ignore_errors: yes

    - name: "Enable ingress"
      ansible.builtin.shell:
        cmd: "minikube addons enable ingress"
      become: no

    - name: "Delete validation manifest"
      ansible.builtin.shell:
        cmd: "kubectl delete ValidatingWebhookConfiguration ingress-nginx-admission"
      become: no

    - name: "Apply ingress"
      ansible.builtin.shell:
        cmd: "kubectl apply -f /vagrant/services/ingress.yaml"
      become: no

    - name: "Get IP"
      ansible.builtin.shell:
        cmd: "minikube ip"
      register: ip
      become: no

    - name: "Add host"
      ansible.builtin.shell:
        cmd: "echo \"{{ip.stdout}} webapp.mds\" | sudo tee -a /etc/hosts"
      become: no